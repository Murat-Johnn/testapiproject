from django.db.models import Q
from rest_framework import status, mixins, viewsets
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from account.models import MyUser
from account.permissions import IsUserPermission, IsSuperPermission
from account.serializers import RegisterSerializer, LoginSerializer, UserSerializer


class RegistrationView(APIView):
    def post(self, request):
        data = request.data
        serializer = RegisterSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response('Successfully registered', status=status.HTTP_201_CREATED)


class LoginView(ObtainAuthToken):
    serializer_class = LoginSerializer


class LogoutView(APIView):
    permission_classes = [IsAuthenticated, ]

    def post(self, request):
        user = request.user
        Token.objects.filter(user=user).delete()
        return Response('Successfully logged out', status=status.HTTP_200_OK)


class UserViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, mixins.DestroyModelMixin, mixins.UpdateModelMixin,
                  viewsets.GenericViewSet):
    queryset = MyUser.objects.all()
    serializer_class = UserSerializer
    lookup_field = 'phone_number'
    lookup_url_kwarg = 'phone_number'

    def get_permissions(self):
        if self.request.user.is_superuser:
            permissions = [IsAuthenticated, IsSuperPermission]
        elif self.action in ['retrieve', 'partial_update', 'destroy', 'update']:
            permissions = [IsAuthenticated, IsUserPermission ]
        else:
            permissions = [IsAuthenticated]
        return [permission() for permission in permissions]

    def get_serializer_context(self):
        return {'request': self.request, 'action': self.action}

    @action(detail=False, methods=['get'])  # router build path post/search/?q=phone_number
    def search(self, request, pk=None):
        q = request.query_params.get('q')
        print(q)
        queryset = self.get_queryset()
        print(queryset)
        queryset = queryset.filter(Q(phone_number__icontains=q) | Q(username__icontains=q))
        serializer = UserSerializer(queryset, many=True, context={'request': request, 'action': self.action})
        return Response(serializer.data, status=status.HTTP_200_OK)
